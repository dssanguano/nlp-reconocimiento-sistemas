import React from 'react';
import './MisListasContent.css';

function MisListasContent({ sentimientos }) {
  if (!sentimientos) {
    return null;
  }

  return (
    <div className="mis-listas-content">
      <h2>Mis Listas de Sentimientos</h2>
      <ul>
        {sentimientos.map((sentimiento, index) => (
          <li key={index}>{sentimiento}</li>
        ))}
      </ul>
    </div>
  );
}

export default MisListasContent;