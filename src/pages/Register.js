import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import './Register.css'; 

function Register() {
  const [name, setName] = useState('');
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleRegister = (e) => {
    e.preventDefault();
    console.log('Registrar usuario:', name, username, password);
  };

  return (
    <div className="register-container">
      <div className="overlay">
        <h2>Registrarse</h2>
        <form onSubmit={handleRegister}>
          <div>
            <label>Nombre:</label>
            <input type="text" value={name} onChange={(e) => setName(e.target.value)} />
          </div>
          <div>
            <label>Nombre de Usuario:</label>
            <input type="text" value={username} onChange={(e) => setUsername(e.target.value)} />
          </div>
          <div>
            <label>Contraseña:</label>
            <input type="password" value={password} onChange={(e) => setPassword(e.target.value)} />
          </div>
          <button type="submit">Registrarse</button>
        </form>
        <p>¿Ya tienes una cuenta? <Link to="/login">Iniciar Sesión</Link></p>
        <p><Link to="/">Regresar a la página principal</Link></p>
      </div>
    </div>
  );
}

export default Register;


