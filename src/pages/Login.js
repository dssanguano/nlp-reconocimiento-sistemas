import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import './Login.css'; 

function Login() {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleLogin = (e) => {
    e.preventDefault();
    console.log('Iniciar sesión con:', username, password);
  };

  return (
    <div className="login-container">
      <div className="overlay">
        <h2>Iniciar Sesión</h2>
        <form onSubmit={handleLogin}>
          <div>
            <label>Nombre de Usuario:</label>
            <input type="text" value={username} onChange={(e) => setUsername(e.target.value)} />
          </div>
          <div>
            <label>Contraseña:</label>
            <input type="password" value={password} onChange={(e) => setPassword(e.target.value)} />
          </div>
          <Link to="/dashboard" className="boton">Iniciar Sesión</Link>
        </form>
        <p>¿No tienes una cuenta? <Link to="/register">Regístrate</Link></p>
        <p><Link to="/">Regresar a la página principal</Link></p>
      </div>
    </div>
  );
}

export default Login;


