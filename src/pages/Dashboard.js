import React, { useState } from 'react';
import { Link, Outlet } from 'react-router-dom';
import HomeContent from './HomeContent';
import BusquedaContent from './BusquedaContent';
import './Dashboard.css';
import { confirmAlert } from 'react-confirm-alert';

function Dashboard() {
    // Estado para controlar la visibilidad del menú lateral
    const [menuVisible, setMenuVisible] = useState(false);

    // Función para alternar la visibilidad del menú
    const toggleMenu = () => {
        setMenuVisible(!menuVisible);
    };

    // Función para manejar el cierre de sesión
    const handleLogout = () => {
        confirmAlert({
            title: 'Confirmar',
            message: '¿Estás seguro de que deseas cerrar sesión?',
            buttons: [
                {
                    label: 'Cancelar',
                    onClick: () => console.log('Se canceló la acción')
                },
                {
                    label: 'Confirmar',
                    onClick: () => {
                        console.log('Se confirmó la acción');
                        // Realiza la redirección al hacer clic en "Confirmar"
                        window.location.href = '/login';
                    }
                }
            ]
        });
    };

    return (
        <div className="dashboard-container">
            {/* Menú lateral */}
            <div className={`menu ${menuVisible ? 'visible' : ''}`}>
                <p>Diego Noguera</p>
                <Link to="/dashboard">Inicio</Link>
                <Link to="/dashboard/buscar">Buscar</Link>
                <Link to="/dashboard/mislistas">Mis Sentimientos</Link>
                <Link className="logout-button" onClick={handleLogout}>Salir</Link>
            </div>
            {/* Contenido principal */}
            <div className="content">
                <button className="toggle-menu" onClick={toggleMenu}>
                    ☰
                </button>
                {/* Utiliza el componente Outlet para manejar las rutas anidadas */}
                <Outlet />
            </div>
        </div>
    );
}

export default Dashboard;

