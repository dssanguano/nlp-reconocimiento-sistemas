import { useParams } from 'react-router-dom';

function ReproductorContent() {
  const { listaId } = useParams();

  const decodedHref = decodeURIComponent(listaId);

  return (
    <div className="reproductor-content">
      <iframe title="Reproductor de Lista" src={decodedHref} width="100%" height="400" />
    </div>
  );
}

export default ReproductorContent;
