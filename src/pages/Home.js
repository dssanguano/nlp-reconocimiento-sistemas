import React from 'react';
import { Link } from 'react-router-dom';
import './Home.css';
import Logo from "../images/Logo.jpg";

function Home() {
    return (
        <div className="home-container">
            <div className="overlay">
                <img src={Logo} alt="Logo" />
                <h1>ESCUCHA LA MÚSICA QUE SIENTES</h1>
                <p>Descubre nueva música gracias a la IA</p>
                <div className="buttons">
                    <Link to="/register" className="register-button">
                        Registrarse
                    </Link>
                    <Link to="/login" className="login-button">
                        Iniciar Sesión
                    </Link>
                </div>
            </div>
        </div>
    );
}

export default Home;
