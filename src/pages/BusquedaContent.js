import React, { useState } from 'react';
import './BusquedaContent.css';
import MisListasContent from './MisListasContent';

function BusquedaContent() {
  // Estado para el texto de búsqueda, respuesta del servidor, texto de voz y sentimientos
  const [searchText, setSearchText] = useState("");
  const [response, setResponse] = useState(null);
  const [voiceText, setVoiceText] = useState("");
  const [sentimientos, setSentimientos] = useState([]);

  // Función para analizar el sentimiento del texto ingresado
  const analizarSentimiento = () => {
    const apiUrl = "http://localhost:8000/analizar_sentimiento/";

    const requestBody = {
      texto: searchText
    };

    fetch(apiUrl, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(requestBody)
    })
      .then(response => response.json())
      .then(data => {
        setResponse(data);
        setSentimientos([...sentimientos, response.sentimiento]);
      })
      .catch(error => {
        console.error("Error al realizar la solicitud:", error);
      });
  };

  // Función para obtener listas de reproducción basadas en un sentimiento
  const sentimientoBoton = (sentimiento) => {
    fetch('http://localhost:8000/sentimiento/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ texto: sentimiento })
    })
      .then(response => response.json())
      .then(data => {
        setResponse(data);
        setSentimientos([...sentimientos, response.sentimiento]);
      })
      .catch(error => {
        console.error('Error al realizar la solicitud:', error);
      });
  };

  // Función para iniciar el reconocimiento de voz
  const startVoiceRecognition = () => {
    const recognition = new (window.SpeechRecognition || window.webkitSpeechRecognition)();
    recognition.lang = "es-ES";

    recognition.onresult = (event) => {
      const transcript = event.results[0][0].transcript;
      setVoiceText(transcript);
      setSearchText(transcript); // Establece el texto reconocido en el input de búsqueda
    };

    recognition.start();
  };

  return (
    <div className="busqueda-content">
      <h2>¿Cómo te sientes hoy?</h2>
      <input
        type="text"
        placeholder="O... ¿Qué hiciste hoy?"
        value={searchText}
        onChange={(e) => setSearchText(e.target.value)}
      />
      <div className='micro'>
        <p>¡Háblame!</p>
      </div>
      <button className="voice-button" onClick={startVoiceRecognition}>
        <i className="fas fa-microphone"></i>
      </button>
      <button className="buscar-boton" onClick={analizarSentimiento}>
        Buscar
      </button>
      {/* Mostrar la respuesta del servidor si está disponible */}
      {response && response.sentimiento && (
        <div className="respuesta">
          {response.sentimiento !== null && (
            <p>Sentimiento: {response.sentimiento}</p>
          )}
          <p>Listas de Reproducción:</p>
          <ul>
            {response.listas_reproduccion.map((lista, index) => (
              <li key={index}>
                <a href={lista.href} target="_blank" rel="noopener noreferrer">
                  {lista.name}
                </a>
              </li>
            ))}
          </ul>
        </div>
      )}
      {/* Botones circulares para sentimientos */}
      <div className="botones-circulares">
        <button className="boton-circular feliz" onClick={() => sentimientoBoton('Feliz')}>😄Feliz</button>
        <button className="boton-circular triste" onClick={() => sentimientoBoton('Triste')}>😢Triste</button>
        <button className="boton-circular fiesta" onClick={() => sentimientoBoton('Fiesta')}>🎉Fiesta</button>
        <button className="boton-circular relajacion" onClick={() => sentimientoBoton('Relajacion')}>😌Relajación</button>
      </div>
      {/* Componente para mostrar listas de reproducción basadas en sentimientos */}
      <MisListasContent sentimientos={response && response.listas_reproduccion ? response.listas_reproduccion.map(lista => lista.name) : []} />
    </div>
  );
}

export default BusquedaContent;
