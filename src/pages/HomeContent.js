import React from 'react';
import Logo from "../images/Logo.jpg";

function HomeContent() {
  return (
    <div className="home-content">
      <img src={Logo} alt="Logo" />
      <h1>ESCUCHA LA MÚSICA QUE SIENTES</h1>
      <p>Este proyecto nació con la finalidad de ayudar a las personas con problemas de depresión.</p>
      <p>La música puede tener un impacto profundo en tu estado de ánimo y gracias a nosotros encontrarás nueva música.</p>
      <p>Y recuerda...</p>
      <h2>No estas solo!</h2>
    </div>
  );
}

export default HomeContent;
