import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';
import Dashboard from './pages/Dashboard';
import HomeContent from './pages/HomeContent';
import BusquedaContent from './pages/BusquedaContent';
import MisListasContent from './pages/MisListasContent';

import './App.css';

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Register />} />
        <Route path="/dashboard" element={<Dashboard />}>
          <Route index element={<HomeContent />} />
          <Route path="/dashboard/buscar" element={<BusquedaContent />} />
          <Route path="/dashboard/mislistas" element={<MisListasContent />} />
        </Route>
      </Routes>
    </Router>
  );
}

export default App;

